# Experimental simulator for Mbed OS 5 applications

**Demo: https://labs.mbed.com/simulator**

![Screenshot](https://os.mbed.com/media/uploads/janjongboom/simulator2.png)

### Docker installation

1. Download and Install Docker (https://docs.docker.com/desktop/)
1. Build the Docker image:  
    `docker build -t mbed/simulator .`
1. Run the Docker image:  
    `docker run -d -p 8002:7829 mbed/simulator`
1. The simulator can now be accessed at  
    http://localhost:8002

